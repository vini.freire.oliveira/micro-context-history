import time, sys, os
from timeloop import Timeloop
from datetime import timedelta

from services.consumer import consumer

tl = Timeloop()

@tl.job(interval=timedelta(seconds=5))
def main():
  consumer()
    
if __name__ == "__main__":
  tl.start(block=True)
  try:
    main()
  except KeyboardInterrupt:
    print('Interrupted')
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)
    