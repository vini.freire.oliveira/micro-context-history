from pymongo import MongoClient
import json


def operation(body):
  client = MongoClient('database_context_history')
  db = client['context_history']
  history = db['histories']

  msg = json.loads(body)

  obj_id = history.insert_one(msg['message']).inserted_id
  print(" history saved %r" % obj_id)
