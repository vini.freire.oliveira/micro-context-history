#!/usr/bin/env python
import pika

from operations.database import operation

def consumer():
  connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
  channel = connection.channel()

  channel.queue_declare(queue='context-history')

  def callback(ch, method, properties, body):
    operation(body)

  channel.basic_consume(queue='context-history', on_message_callback=callback, auto_ack=True)

  print(' [*] Waiting for messages. To exit press CTRL+C')
  channel.start_consuming()

